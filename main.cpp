#include "windmill.hpp"
#include <iostream>

using namespace std;
using namespace cv;

Point center(Rect lunkuo)
{
	// Point up,down;
	// float radius;
	// up.x=lunkuo[0].x;
	// up.y=lunkuo[0].y;
	// down.x=lunkuo[0].x;
	// down.y=lunkuo[0].y;
	// for(int j=1;j<lunkuo.size();j++)
	// {
	// 	if(lunkuo[j].x<up.x) up.x=lunkuo[j].x;
	// 	if(lunkuo[j].y>up.x) up.y=lunkuo[j].x;
	// 	if(lunkuo[j].x>down.x) down.x=lunkuo[j].x;
	// 	if(lunkuo[j].y<down.x) down.y=lunkuo[j].x;

	// }
	// // if(up.y-down.y>down.x-up.x) radius=(up.y-down.y)/2;
	// // else radius=(down.x-up.x)/2;
	Point center((lunkuo.tl().x+lunkuo.br().x) / 2,((lunkuo.tl().y+lunkuo.br().y)/ 2));
	cout<<center<<endl;
	
	return center;
}

void getContours(Mat imgDil,Mat img)
{
	//创建轮廓
	vector<vector<Point>> contours;
	//创建层级轮廓
	//Vec4i:每个向量具有4个整数值
	vector<Vec4i> hierarchy;
	//找到轮廓
	findContours(imgDil, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	
	//绘制轮廓
	//绘制所有的，符号为-1,颜色为紫色，厚度为2
	//drawContours(img, contours, -1,Scalar(255, 0, 255),10);
    
	//过滤噪声点
	
	for (int i = 0; i < contours.size(); i++)
	{
		//先找到每个轮廓的面积
		int area = contourArea(contours[i]);
		//打印出面积
		cout << area << endl;
		//定义轮廓对象的名称
		string objectType;
		//角点向量，仅存储轮廓中的角点 
		vector<vector<Point>> conPoly(contours.size());
		//存储矩形边界框向量
		vector<Rect> boundRect(contours.size());
		
		//如果面积在可提取范围内，则提取对应轮廓
		
		//找到每个轮廓的周长
		float peri = arcLength(contours[i], true);
		//从轮廓中找到曲线的角点，true表示是否闭合
		//轮廓：contours[i]中包含所有的点
		//而角点：conPoly[i]中仅包含角点
		approxPolyDP(contours[i], conPoly[i], 0.02 * peri,true);
		//绘制所有的，符号为-1,颜色为紫色，厚度为2
		// drawContours(img, conPoly, i, Scalar(255, 0, 255), 2);
		//打印出每个轮廓的角点数
		// cout << conPoly[i].size() << endl; 
		//定义每个轮廓的角点数
		int objCor = (int)conPoly[i].size();
		//绘制矩形边界框，将含纳每一个独立的形状
		boundRect[i] = boundingRect(conPoly[i]);

		
		//R判断
		if (area<1000 && objCor>5)
		{
			objectType = "R";
			circle(img, center(boundRect[i]), 20,Scalar(160, 80, 86),LINE_8,1, 0);
			
		}
		//Hammer判断
		else if(500 < area && area<6000)
		{

			objectType = "Hammer";
			// putText(img, objectType, { boundRect[i].x,boundRect[i].y - 5 }, FONT_HERSHEY_DUPLEX, 0.75, Scalar(0, 69, 255), 2);
			//将边界框打印在原图上
			rectangle(img, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 255, 0), 5);
			
		}
		putText(img, objectType, { boundRect[i].x,
		boundRect[i].y - 5 }, FONT_HERSHEY_DUPLEX, 0.75, Scalar(0, 69, 255), 2);
	}
}

int main()
{
    std::chrono::milliseconds t = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    WINDMILL::WindMill wm(t.count());
    cv::Mat src;
    while (1)
    {
        t = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
        src = wm.getMat((double)t.count()/1000);
        
        //==========================代码区========================//
        
        //预处理，找到图像的边缘
        //找到边缘，然后找到轮廓/轮廓点
        //1.转为灰度
        Mat imgGray;
        cvtColor(src, imgGray, COLOR_BGR2GRAY);
        //2.添加高斯模糊
        Mat imgBlur;
        GaussianBlur(imgGray, imgBlur, Size(3, 3), 3, 0);
        //3.进行坎尼边缘检测
        Mat imgCanny;
        Canny(imgBlur, imgCanny, 25, 75);
        //4.得到轮廓
        getContours(imgCanny,src);
        imshow("windmill", src);

        //=======================================================//
        
        waitKey(1);
    }
}